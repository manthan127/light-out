# Light Out



## Getting started
- I/P. 
  - When the game starts, a random lights is switched on in Grid. 
- Process. 
  - Pressing any of the lights will toggle it and the adjacent lights. 
- O/P
  - The goal of the puzzle is to switch all the lights off.

- [ ] [Lights Out Classic](#lights-out-classic)

- [ ] [Lights Out Mini](#lights-out-mini)

- [ ] [Lights Out Deluxe Plus](#lights-out-deluxe-plus)

- [ ] [Lights Out Deluxe Cross](#lights-out-deluxe-cross)

- [ ] [Lights Out Cube](#lights-out-cube)


## Lights Out Classic
 
- [ ] I/P. 
 - When the game starts, a random lights is switched on in 5 x 5 Grid. 

- [ ] Process
 - On Click Any switch, pressed switch toggle & it toggle 4 neighbor switch also.
 - On boundary/Corner, where there not all 4 neighbor; only available neighbor will toggle.
 
- [ ] Solve
 - 1). for All top 4 rows, one-by-one turn Off lights by clicking bottom of on light. do not Click on first Row.
 - 2). remaining last line will handle by
   
| Only if first in last row is On,click on this 2 | Only if second in last row is On, click on this 2 | Only if third in last row is On, click on this 1 |
|:---:|:---:|:---:|
| ![asd](/1-1.png) | ![asd](/1-2.png) |![asd](/1-3.png) |

 - 3). Again start Step 1.

 <img src="https://docs.google.com/drawings/d/e/2PACX-1vTQuRhcOleTFw4-vKXZzEweuMxWXdv4OHZpPmMM0XKOQq-QOX19riB2u4yX6rOmKevyjHf2zV7kX7Dw/pub?w=500&amp;h=500">

## Lights Out Mini
 
- [ ] I/P. 
 - When the game starts, a random lights is switched on in 4 x 4 Grid. 

- [ ] Process
 - On Click Any switch, pressed switch toggle & it toggle 4 neighbor switch also.
 - On boundary/Corner, where there not all 4 neighbor; wrapped neighbor will toggle.
 
- [ ] Solve
 - 1). Try to solve as many of the lights as possible by a few button presses.
 - 2). in remaining last lines; if you want to turn off any light, just click on it & all it's 4 wrapped neighbors also.

| Corner Cell | Edge Cell | Normal Cell |
|:---:|:---:|:---:|

<img src="https://docs.google.com/drawings/d/e/2PACX-1vTI-FUwnRADQRduU4eMPhnuDrA-qjzVI-yLSeIZfqwPF8qrHR1wTdNaat4eDMSen5aefOA5Lq0AzKwH/pub?w=500&amp;h=500">

## Lights Out Deluxe Plus

<img src="https://docs.google.com/drawings/d/e/2PACX-1vSx1XXLdgbL839D2BTvnD1q7YBCAbVVGvJGoFrT57BxSxSald9QbNrZzGMo-omIzm9qza4kL5Rt7jbY/pub?w=500&amp;h=500">


## Lights Out Deluxe Cross

<img src="https://docs.google.com/drawings/d/e/2PACX-1vRirzR6GHjZuakUPHI8vGswjHonyRXoQveCJgQOQQ0bRiYSlxQSEv45a0O_sfHc0kvJWSeKpMCNmI7N/pub?w=500&amp;h=500">

## Lights Out Cube

<img src="https://docs.google.com/drawings/d/e/2PACX-1vSHimxZ_ET5D9d1wvC8zgqx_l6WEkGti3chRLEnJsEIXXQy44te2ltIbBlsxP8EJApd9eHrfDY-MXKH/pub?w=1500&amp;h=1500">


## License
Open source projects.

## Project status
...
